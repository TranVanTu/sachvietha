@extends('master')
@section('content1')
<div class="inner-header">
		<div class="container">
			<div class="pull-left">
				<u><h6 class="inner-title">Liên hệ</h6></u>
			</div>
			<div class="pull-right">
				<div class="beta-breadcrumb font-large">
					<a href="index.html">Home</a> / <span>Liên Hệ</span>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="beta-map">
		
		<div class="abs-fullwidth beta-map wow flipInX">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1863.126719159684!2d106.05989737909037!3d20.942335042320234!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135a47e38341dbb%3A0x5a68a20bebbd4894!2zVHLGsOG7nW5nIMSQSFNQS1QgSMawbmcgWcOqbg!5e0!3m2!1svi!2s!4v1589427984383!5m2!1svi!2s" width="300" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>
	</div>
	<div class="container">
		<div id="content" class="space-top-none">
			
			<div class="space50">&nbsp;</div>
			<div class="row">
				
				<div class="col-sm-4">
					<u><h2>Thông tin liên hệ</h2></u>
					<div class="space20">&nbsp;</div>

					<u><h6 class="contact-title">Địa chỉ</h6></u>
					<p>
						Trường Đại Học Sư phạm kỹ thuật Hưng Yên Cơ Sở 2 <br>
						Mỹ Hào - Hưng Yên - Việt Nam.<br>
					</p>
					<div class="space20">&nbsp;</div>
					<u><h6 class="contact-title">Email</h6></u>
					<p>
						VietHa19988@gmail.com <br>
						
					</p>
					<div class="space20">&nbsp;</div>
					<u><h6 class="contact-title">Số Điện Thoại</h6></u>
					<p>
						0824589992<br>
					</p>
				</div>
			</div>
		</div> <!-- #content -->
	</div> <!-- .container -->
	@endsection
