	<div id="footer" class="color-div">
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
					<div class="widget">
						<h4 class="widget-title">Map</h4>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1863.126719159684!2d106.05989737909037!3d20.942335042320234!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135a47e38341dbb%3A0x5a68a20bebbd4894!2zVHLGsOG7nW5nIMSQSFNQS1QgSMawbmcgWcOqbg!5e0!3m2!1svi!2s!4v1589427984383!5m2!1svi!2s" max-width="400" max-height="300" width="auto" height="auto" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="widget">
						<h4 class="widget-title">Thông tin</h4>
						<div>
							<ul>
								<li><a href="{{route('dangky')}}"><i class="fa fa-chevron-right"></i>Đăng Ký</a></li>
								<li><a href="{{route('dangnhap')}}"><i class="fa fa-chevron-right"></i> Đăng Nhập</a></li>
								<li><a href="{{route('trang-chu')}}"><i class="fa fa-chevron-right"></i> Trang Chủ</a></li>
								<li><a href="{{route('gioithieu')}}"><i class="fa fa-chevron-right"></i> Giới Thiệu</a></li>
								<li><a href="{{route('lienhe')}}"><i class="fa fa-chevron-right"></i> Liên Hệ</a></li>
								
							</ul>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
				 <div class="col-sm-10">
					<div class="widget">
						<h4 class="widget-title">Liên hệ với chúng tôi</h4>
						<div>
							<div class="contact-info">
								<i class="fa fa-map-marker"></i>
								<p>Trương Việt Hà <br/> Phone: +84 824589992</p>
								
							</div>
                        <div  class="contact-info"> Khoa cntt. Trường Địa chỉ: Đại Học Sư Phạm Kỹ Thuật Hưng Yên Cơ Sở 2</div>
						</div>
					</div>
				  </div>
				</div>
			</div>
		</div>
	</div> <!-- #footer -->
	<div class="copyright">
		<div class="container">
			<p class="pull-left">Privacy policy. (&copy;) 2020</p>
			
			<div class="clearfix"></div>
		</div>
	</div>
